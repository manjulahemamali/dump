
- SOC :- SM4250
- Model :- T671H
- Code Name :- Austin
- Android Version :- 12
- VNDK :- 30
- Build Number :-  TCL/T671H_EEA/Austin:12/SKQ1.211006.001/Q5HB:user/release-keys
- Security Patch :- 2022-10-01
- FULL-OTA Link :- https://g2slave-ap-south-01.tclcom.com/d935b4b1127fbcb17d4f07672088ae1d4fb3b09a/15/834115
