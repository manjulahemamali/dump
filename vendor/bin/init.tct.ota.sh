#! /vendor/bin/sh

#Set OTA special ver
memory_type=`getprop ro.boot.memory_type`
echo "memory_type: $memory_type"
case "$memory_type" in
    "EMMC_DDR4")
        setprop ro.vendor.fota.special.ver 0
        ;;
    "UFS")
        setprop ro.vendor.fota.special.ver 1
        ;;
    "EMMC_DDR3")
        setprop ro.vendor.fota.special.ver 2
        ;;
    *)
        ;;
esac
