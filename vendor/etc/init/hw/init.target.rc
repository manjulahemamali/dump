# Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

# Added by jinming.xiang for 10147350
import /vendor/etc/init/hw/init.${ro.build.product}.rc

on early-init
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules audio_q6_pdr audio_q6_notifier audio_snd_event audio_apr audio_adsp_loader audio_q6 audio_native audio_usf audio_pinctrl_lpi audio_swr audio_swr_ctrl audio_platform audio_stub audio_wcd_core audio_wcd9xxx audio_wsa881x_analog audio_bolero_cdc audio_va_macro audio_rx_macro audio_tx_macro audio_mbhc audio_wcd937x audio_wcd937x_slave audio_pm2250_spmi audio_rouleur audio_rouleur_slave audio_machine_bengal
    write /proc/sys/kernel/sched_boost 1
    exec u:r:vendor_qti_init_shell:s0 -- /vendor/bin/init.qti.early_init.sh
    setprop ro.soc.model ${ro.vendor.qti.soc_model}

on init
    write /dev/stune/foreground/schedtune.sched_boost_no_override 1
    write /dev/stune/top-app/schedtune.sched_boost_no_override 1
    write /dev/stune/schedtune.colocate 0
    write /dev/stune/background/schedtune.colocate 0
    write /dev/stune/system-background/schedtune.colocate 0
    write /dev/stune/foreground/schedtune.colocate 0
    write /dev/stune/top-app/schedtune.colocate 1
    #Moving to init as this is needed for qseecomd
    wait /dev/block/platform/soc/${ro.boot.bootdevice}
    symlink /dev/block/platform/soc/${ro.boot.bootdevice} /dev/block/bootdevice
    start vendor.qseecomd
    start keymaster-4-0
    #TCT tianhongwei add for aw881xx calibration
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0034/f0
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0034/re
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0034/rw
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0034/monitor
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0036/f0
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0036/re
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0036/rw
    chown system system /sys/bus/i2c/drivers/aw881xx_smartpa/0-0036/monitor
    chown system system /sys/devices/platform/soc/4a88000.i2c/i2c-1/1-0038/fts_test
    chown system system /sys/devices/platform/soc/soc:qcom,dsi-display-primary/panel_calibrate

on early-fs
    start vold

on fs
    start hwservicemanager
    mount_all --early
    chown root system /mnt/vendor/persist
    chmod 0771 /mnt/vendor/persist
    restorecon_recursive /mnt/vendor/persist
    mkdir /mnt/vendor/persist/data 0700 system system

    #chown system oempersist /oempersist
    #chmod 0771 /oempersist
    #restorecon_recursive /oempersist

    #MODIFIED-BEIGN by hengbing.wu, 2020-10-10,defect:10011677
    chown system oempersist /oempersist
    chmod 0771 /oempersist
    restorecon_recursive /oempersist
    mkdir /oempersist/display 0771 system graphics
    mkdir /oempersist/data 0711 root root
    mkdir /oempersist/data/param 0711 root root
    write /oempersist/data/param/macaddr "00:00:00:00:00:00"
    chmod 0644 /oempersist/data/param/macaddr
    #MODIFIED-END by hengbing.wu, 2020-10-10,defect:10011677

    #Begin-added by wenhui.xu 2021/01/13 for omadm db on Tsak:10642453.
    mkdir /oempersist/omadm 0771 system system
    #Begin-added by wenhui.xu 2021/01/13 for omadm db on Tsak:10642453.

#Begin modify by lanying.he for XR10016042 on 2020/09/29
    mkdir /oempersist/audio 0771 root system
    chown root system /oempersist/audio/aw_cali.bin
    chmod 664 /oempersist/audio/aw_cali.bin
#End modify by lanying.he for XR10016042 on 2020/09/29
#BEGIN Added by xiongbo.huang for defect 10011909 on 2020-11-04
    chown system system /sys/class/tct_touch/gesture/double_wakeup_enable
#END Added by xiongbo.huang for defect 10011909 on 2020-11-04

    # New app preload partition
    #chown system system /preload
    #chmod 0771 /preload
    #restorecon_recursive /preload


on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

on late-fs
    wait_for_prop hwservicemanager.ready true
    exec_start wait_for_keymaster
    mount_all --late

on post-fs-data
    mkdir /vendor/data/tombstones 0771 system system
    mkdir /data/vendor/tloc 0700 system drmrpc
    #Begin added by kaiyi.chen for task 9824908 on 2020-08-30
    load_oem_props
    #End added by kaiyi.chen for task 9824908 on 2020-08-30

# add start by jiancong.huang richland/t1_le power on at 2020.12.31
on post-fs-data
    mkdir /data/vendor/goodix 0771 system system
    mkdir /data/vendor/goodix/gf_data 0771 system system
    mkdir /data/system/goodix 0771 system system
    mkdir /data/system/goodix/gf_data 0771 system system
# add end by jiancong.huang richland/t1_le power on at 2020.12.31

on early-boot
    start vendor.sensors

on boot
    write /dev/cpuset/audio-app/cpus 1-2
#USB controller configuration
    setprop vendor.usb.rndis.func.name "gsi"
    setprop vendor.usb.rmnet.func.name "gsi"
    setprop vendor.usb.rmnet.inst.name "rmnet"
    setprop vendor.usb.dpl.inst.name "dpl"
    setprop vendor.usb.qdss.inst.name "qdss"
    setprop vendor.usb.controller 4e00000.dwc3
#Begin added by hengbing.wu for Defect10051472 on 2020.11.5
#Load WLAN driver
    insmod /vendor/lib/modules/qca_cld3_wlan.ko
#End added by hengbing.wu for Defect10051472 on 2020.11.5

#Begin meng.zhang VoIP distortion for defect 10412995 on 2020/12/21
    write /dev/cpuset/audio-app/cpus 1-2
#End meng.zhang VoIP distortion for defect 10412995 on 2020/12/21

#MODIFIED-BEGIN by bo.yu, 2021-02-01,task-10694478
    chmod 0666 /dev/i2c-0
    chown audio system sys/module/snd_soc_tfa98xx_tct/parameters/fw_name
    chown audio system sys/class/ftm_cal/enable_bitclk
    chmod 0666 /dev/aw882xx_smartpa_l
    chmod 0666 /dev/aw882xx_smartpa_r
#MODIFIED-END by bo.yu, 2021-02-01,task-10694478

#Begin added by weiqiang.zhou for NFC DeviceINfor, FR 10427452
    chown nfc /sys/class/deviceinfo/device_info/NFC
#end added by weiqiang.zhou for NFC DeviceINfor, FR 10427452
    #Add by peisong.cao for t1_le hbm
    chown system system /sys/devices/platform/soc/soc:qcom,dsi-display-primary/hbm


#Begin zihao.li for [Task][9733010] Add sensor info & otp check on 20200824
on post-fs-data
    chmod 0666 /sys/class/deviceinfo/device_info/CamNameB
    chmod 0666 /sys/class/deviceinfo/device_info/CamNameB2
    chmod 0666 /sys/class/deviceinfo/device_info/CamNameB3
    chmod 0666 /sys/class/deviceinfo/device_info/CamNameB4
    chmod 0666 /sys/class/deviceinfo/device_info/CamNameF
    chmod 0666 /sys/class/deviceinfo/device_info/CamOTPB
    chmod 0666 /sys/class/deviceinfo/device_info/CamOTPB2
    chmod 0666 /sys/class/deviceinfo/device_info/CamOTPF
    chmod 0666 /sys/devices/platform/soc/5c1b000.qcom,cci0/5c1b000.qcom,cci0:qcom,eeprom0/calibration_config
#End   zihao.li for [Task][9733010] Add sensor info & otp check on 20200824

#add by ancheng.wang for MegviiHDR model path and file RICHOSUP-2816 20220615 begain
on boot && property:persist.vendor.camera.hdr_ready=""
    #write /dev/kmsg "do anc hdryuv copy on first boot"
    #Create folder of camera for anc yuvhdr model
    mkdir /data/vendor/camera/anc_yuvhdr 0777 cameraserver audio
    copy /vendor/etc/camera/anc_yuvhdr_algo_cache /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_algo_cache
    copy /vendor/etc/camera/anc_yuvhdr_binary_cache /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_binary_cache
    copy /vendor/etc/camera/anc_yuvhdr_model /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_model
    copy /vendor/etc/camera/anc_yuvhdr_params.json /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_params.json
    #set user group for cameraserver
    chown cameraserver audio /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_algo_cache
    chown cameraserver audio /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_binary_cache
    chown cameraserver audio /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_model
    chown cameraserver audio /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_params.json
    #set file mode
    chmod 0666 /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_algo_cache
    chmod 0666 /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_binary_cache
    chmod 0666 /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_model
    chmod 0666 /data/vendor/camera/anc_yuvhdr/anc_yuvhdr_params.json
    setprop persist.vendor.camera.hdr_ready 1
#add by ancheng.wang for MegviiHDR model path and file RICHOSUP-2816 20220615 end

#Begin ersen.shang for [Task][9733010] Add sensor info & otp check on 20200824
#ActivityManagerService start
on property:sys.boot_completed=1
    chmod 0666 /sys/devices/platform/soc/5c1b000.qcom,cci0/5c1b000.qcom,cci0:qcom,eeprom0/calibration_data

#End   ersen.shang for [Task][9733010] Add sensor info & otp check on 20200824


on boot && property:persist.vendor.usb.controller.default=*
    setprop vendor.usb.controller ${persist.vendor.usb.controller.default}

on property:vendor.usb.controller=*
    setprop sys.usb.controller ${vendor.usb.controller}

on charger
    start vendor.power_off_alarm
    setprop vendor.usb.controller 4e00000.dwc3
    setprop sys.usb.configfs 1

#pd-mapper
service vendor.pd_mapper /vendor/bin/pd-mapper
    class core
    user system
    group system

#Peripheral manager
service vendor.per_mgr /vendor/bin/pm-service
    class core
    user system
    group system
    ioprio rt 4

service vendor.per_proxy /vendor/bin/pm-proxy
    class core
    user system
    group system
    disabled

service vendor.mdm_helper /vendor/bin/mdm_helper
    class core
    group system wakelock
    disabled

service vendor.mdm_launcher /vendor/bin/sh /vendor/bin/init.mdm.sh
    class core
    oneshot

on property:init.svc.vendor.per_mgr=running
    start vendor.per_proxy

on property:sys.shutdown.requested=*
    stop vendor.per_proxy

on property:vold.decrypt=trigger_restart_framework
   start vendor.cnss_diag

service vendor.cnss_diag /system/vendor/bin/cnss_diag -q -f -t HELIUM
   class main
   user system
   group system wifi inet sdcard_rw media_rw diag
   oneshot

service dcvs-sh /vendor/bin/init.qti.dcvs.sh
    class late_start
    user root
    group root system
    disabled
    oneshot

#added by weiqiang.zhou for MMI begin
service vendor.focaltech.fps_hal /vendor/bin/hw/vendor.focaltech.fingerprint@1.0-service
    class hal
    user system
    group system
#added by weiqiang.zhou for MMI end

on property:vendor.dcvs.prop=1
   start dcvs-sh

#Begin added by yangao.chen for T9795019 on 2020-08-15
on mmi && property:ro.bootmode=ffbm-00
    setprop persist.vendor.qcomsysd.enabled 1
    write /sys/class/power_supply/battery/charging_enabled 0
on mmi && property:ro.bootmode=ffbm-01
    setprop persist.vendor.qcomsysd.enabled 1
    write /sys/class/power_supply/battery/charging_enabled 0
on mmi && property:ro.bootmode=ffbm-02
    setprop persist.vendor.qcomsysd.enabled 1
    write /sys/class/power_supply/battery/charging_enabled 0
#End added by yangao.chen for T9795019 on 2020-08-15
#Begin added by kaiyi.chen for task 9843535 (FCM SIM configuration) on 2020-09-01
on property:ro.boot.multisim=1
    setprop persist.radio.multisim.config ssss
on property:ro.boot.multisim=3
    setprop persist.radio.multisim.config dsds
#End added by kaiyi.chen for task 9843535 (FCM SIM configuration) on 2020-09-01

#Added by zhijiao.li for task9978001 on 2020-09-15 begin
on property:sys.usb.diagprotect.config=*
    setprop persist.vendor.usb.config ${sys.usb.diagprotect.config}
    setprop sys.usb.config ${sys.usb.diagprotect.config}
#Added by zhijiao.li for task9978001 on 2020-09-15 end

#BEGIN Added by zhijiao.li for task10051536 on 2020-10-13
on boot && property:ro.boot.code=*
    mount ext4 /odm/etc/${ro.boot.code}/permissions /odm/etc/permissions bind
#END Added by zhijiao.li for task10051536 on 2020-10-13

#Begin added by hongwei.tian for MMItest audio loop
service vendor.audio_tctd /vendor/bin/mm-audio-ftm -tc ${debug.audio.tc} -c /vendor/etc/ftm_test_config_audioloop -d 365
    user audio
    group audio
    disabled
    oneshot
#End added by hongwei.tian

# Task: 10669527, Add for roottoken feature
on property:ro.boot.oemtoken=true && property:sys.supervisor=true
    write /sys/module/selinux/parameters/asenforce 1
    write /sys/fs/selinux/enforce 0
    setprop ro.debuggable 1
    restart adbd

# Task: 10717156, for Crash DUMP Flag Management
on property:persist.vendor.dload.enable=*
    write /sys/module/msm_poweroff/parameters/download_mode ${persist.vendor.dload.enable}

# ASTS-5169, allow bootloader enable or disable dump.
on property:ro.boot.download_mode=1
    setprop persist.vendor.ssr.restart_level ALL_DISABLE
    setprop debug.dump2file_x enable

on property:ro.boot.download_mode=0
    setprop persist.vendor.ssr.restart_level ALL_ENABLE
    setprop debug.dump2file_x disable

#ADDED-BEGIN by TCTNB.wen.zhuang, 2020-11-30,BUG-10299817
on property:persist.sys.tct.usb.chgdisabled=1
    write /sys/class/power_supply/battery/charging_enabled 0

on property:persist.sys.tct.usb.chgdisabled=0
    write /sys/class/power_supply/battery/charging_enabled 1
#ADDED-END by TCTNB.wen.zhuang, 2020-11-30,BUG-10299817
