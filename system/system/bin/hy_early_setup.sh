#!/system/bin/sh
#

# kernel does not support
# every else does not matter
if [ ! -e /sys/block/zram0/hyperhold_init ]
then
	exit 0
fi

# this is a one-time job,
# we don't support soft-restart
BOOTED=`getprop "sys.hyperhold.boot_completed"`
if [ "${BOOTED}" == "1" ]
then
	exit 0
fi

setprop "sys.hyperhold.boot_completed" 0

# not enabled, just quit
HYEN=`getprop "persist.sys.hyperhold.enable"`
if [ "${HYEN}" != "1" ]
then
	setprop "sys.hyperhold.boot_reason" 0
	exit 0
fi

# get the disk quota
QUOTA=`getprop "persist.sys.hyperhold.diskquota"`
if [ "${QUOTA}" == "" ]
then
	QUOTA=4718592
	setprop persist.sys.hyperhold.diskquota ${QUOTA}
fi

AVAILABLE=$(df -k | grep "/data$" | awk '{print $4}')

if [ ${AVAILABLE} -lt ${QUOTA} ]; then
	setprop "sys.hyperhold.boot_reason" 2
else
	setprop "sys.hyperhold.boot_reason" 1
fi
