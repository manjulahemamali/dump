#!/system/bin/sh
#

# kernel does not support
# every else does not matter
if [ ! -e /sys/block/zram0/hyperhold_init ]
then
	exit 0
fi

# this is a one-time job,
# we don't support soft-restart
BOOTED=`getprop "sys.hyperhold.boot_completed"`
if [ "${BOOTED}" == "1" ]
then
	exit 0
fi

################################################
# REASON:
#	0,  not enabled
# 	1,  enabled, quota checking pass
#	2,  enabled, quota checking fail
################################################

DISKFILE=/data/hyperhold/diskfile
TARGETFILE=/dev/block/by-name/hyperhold
REASON=`getprop "sys.hyperhold.boot_reason"`

if [ "${REASON}" == "1" ]
then

	RETRY=`getprop "persist.sys.hyperhold.boot_loretry"`
	RETRY_NOSLEEP=`getprop "persist.sys.hyperhold.boot_lonosleep"`
	RETRY_CNT=0
	if [ "${RETRY}" == "" ]; then
		RETRY=5
	fi

	if [ ! -e ${DISKFILE} ]; then
		dd if=/dev/zero of=${DISKFILE} bs=1MB count=1024
	fi

	for iter in `seq 1 ${RETRY}`
	do
		LOOPDEV=`losetup -f -s ${DISKFILE}`

		ERRNO=`echo $?`
		if [ "${ERRNO}" != "0" ]; then
			if [ "${RETRY_NOSLEEP}" != "1" ]; then
				sleep 1
			fi

			RETRY_CNT=`expr ${RETRY_CNT} + 1`
			continue
		fi

		ln -s ${LOOPDEV} ${TARGETFILE}
		setprop "sys.hyperhold.boot_loretry_cnt" ${RETRY_CNT}
		setprop "sys.hyperhold.boot_dev" ${LOOPDEV}

		echo 1 > /sys/block/zram0/hyperhold_init
		echo "loglevel=1" > /sys/block/zram0/hyperhold_ft
		break
	done
else
	if [ -e ${DISKFILE} ]; then
		rm ${DISKFILE}
	fi
fi

setprop "sys.hyperhold.boot_completed" 1
